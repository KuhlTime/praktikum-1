package me.kuhlti.address.controller;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import me.kuhlti.address.Main;
import me.kuhlti.address.util.DateUtil;
import me.kuhlti.address.model.Person;

import javafx.fxml.FXML;

public class PersonOverviewController {

    // ====== Outlets ======

    @FXML private TableView<Person> personTableView;
    @FXML private TableColumn<Person, String> firstNameColumn;
    @FXML private TableColumn<Person, String> lastNameColumn;
    @FXML private ButtonBar buttonBar;
    @FXML private Button addButton;
    @FXML private Button deleteButton;
    @FXML private Button editButton;

    @FXML private GridPane contentGrid;
    @FXML private Label headerLabel;
    @FXML private Label firstNameLabel;
    @FXML private Label lastNameLabel;
    @FXML private Label streetLabel;
    @FXML private Label cityLabel;
    @FXML private Label postalCodeLabel;
    @FXML private Label birthdayLabel;

    private Main main;


    // ====== Initializer ======

    public PersonOverviewController() {}


    // ====== Callbacks ======

    @FXML private void initialize() {
        // Initialize the personTableView
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());

        // Clear person details
        showPersonDetails(null);

        // Add an event listener to the selected item property of the table view.
        personTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showPersonDetails(newValue));
    }

    // ====== Functions ======

    public void setMain(Main main) {
        this.main = main;

        personTableView.setItems(main.getPersons());
    }

    private void showPersonDetails(Person person) {
        boolean personAvailable = (person != null);

        headerLabel.setText(personAvailable ? "Detailansicht" : "Wählen Sie eine Person aus!");
        editButton.setDisable(!personAvailable);
        deleteButton.setDisable(!personAvailable);
        contentGrid.setVisible(personAvailable);

        if (!personAvailable) { return; }

        firstNameLabel.setText(person.getFirstName());
        lastNameLabel.setText(person.getLastName());
        cityLabel.setText(person.getCity());
        streetLabel.setText(person.getStreet());
        postalCodeLabel.setText(Integer.toString(person.getPostalCode()));
        birthdayLabel.setText(DateUtil.format(person.getBirthday()));
    }


    // ====== Actions ======

    @FXML private void handleDeletePerson() {
        int selectedIndex = personTableView.getSelectionModel().getSelectedIndex();
        personTableView.getItems().remove(selectedIndex);

        // Because the delete button is only enabled when a person is selected there is no
        // need to handle the case of no user being selected at this point
    }

    @FXML private void handleNewPerson() {
        Person tempPerson = Person.emptyPerson();
        boolean okClicked = main.showPersonEditDialog(tempPerson);

        if (okClicked) {
            main.getPersons().add(tempPerson);
        }
    }

    @FXML private void handleEditPerson() {
        Person selectedPerson = personTableView.getSelectionModel().getSelectedItem();

        // The edit button is only enabled when a user is selected
        // so there is no need to check if selectedPerson == null

        boolean okClicked = main.showPersonEditDialog(selectedPerson);
        if (okClicked) {
            showPersonDetails(selectedPerson);
        }
    }

}
