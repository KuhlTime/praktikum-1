package me.kuhlti.address.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;


public class Person {

    // ====== Properties ======

    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty city;
    private final StringProperty street;
    private final IntegerProperty postalCode;
    private final ObjectProperty<LocalDate> birthday;


    // ====== Initializer ======

    public Person(String firstName, String lastName, String city, String street, Integer postalCode, LocalDate birthday) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.city = new SimpleStringProperty(city);
        this.street = new SimpleStringProperty(street);
        this.postalCode = new SimpleIntegerProperty(postalCode);
        this.birthday = new SimpleObjectProperty<LocalDate>(birthday);
    }

    public static Person emptyPerson() {
        return new Person("", "", "", "", 0, LocalDate.now());
    }

    public static Person randomPerson() {
        List<String> firstNames = List.of("Seldanna", "Lierin", "Isarrel", "Ailen", "Gorduin", "Ievis");
        List<String> lastNames = List.of("Naexidor", "Fanelis", "Paqen", "Umevyre", "Pagella", "Morlynn");
        List<String> cities =  List.of("Romenna", "Valinor", "Tirion", "Orrostar", "Hyarrostar", "Bruchtal", "Dunland");
        List<String> streets = List.of("Falas", "Taur", "Foro", "Rhûn", "Harad", "Dún", "Londe");

        String firstName = getRandomElementFrom(firstNames);
        String lastName = getRandomElementFrom(lastNames);
        String city = getRandomElementFrom(cities);
        String street = getRandomElementFrom(streets);

        int postalCode = (int) Math.floor(Math.random() * 99999);

        int year = 2020 - (int) Math.floor(Math.random() * 50);
        int dayInYear = (int) Math.floor(Math.random() * 365); // Not respecting leap years
        LocalDate birthday = LocalDate.ofYearDay(year, dayInYear);

        return new Person(firstName, lastName, city, street, postalCode, birthday);
    }


    // ====== Functions ======

    /**
     * Get's an random element from the provided array
     * @param list The list of items from which to pick
     * @param <T> Can be used on any object type -> generic
     * @return Random element of type T
     */
    private static <T> T getRandomElementFrom(List<T> list) {
        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }

    // ====== Computed Props ======

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }


    // Getters

    public String getFirstName() {
        return firstName.get();
    }

    public String getLastName() {
        return lastName.get();
    }

    public String getCity() {
        return city.get();
    }

    public String getStreet() {
        return street.get();
    }

    public int getPostalCode() {
        return postalCode.get();
    }

    public LocalDate getBirthday() {
        return birthday.get();
    }


    // Setters

    public void setFirstName(String value) {
        firstName.set(value);
    }

    public void setLastName(String value) {
        lastName.set(value);
    }

    public void setStreet(String value) {
        street.set(value);
    }

    public void setCity(String value) {
        city.set(value);
    }

    public void setPostalCode(int value) {
        postalCode.set(value);
    }

    public void setBirthday(LocalDate value) {
        birthday.set(value);
    }

}
