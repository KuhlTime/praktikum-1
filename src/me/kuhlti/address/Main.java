package me.kuhlti.address;

import javafx.stage.Modality;
import me.kuhlti.address.model.*;

import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import me.kuhlti.address.controller.PersonEditDialogController;
import me.kuhlti.address.controller.PersonOverviewController;

public class Main extends Application {

    // ====== Properties ======

    private Stage primaryStage;
    private BorderPane rootLayout;

    private ObservableList<Person> persons = FXCollections.observableArrayList();

    // ====== Computed Props ======

    /**
     * Get only property primaryStage.
     * @return Returns the primary stage
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public ObservableList<Person> getPersons() {
        return persons;
    }


    // ====== Initializer ======

    public Main() {
        // Populate persons
        int n = 5;

        for (int i = 0; i < n; i++) {
            persons.add(Person.randomPerson());
        }
    }

    // ====== Functions ======

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Address Buch");

        initRootLayout();
        showPersonOverview();
    }

    /**
     * Initialize the root layout of the application.
     */
    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            Scene scene = new Scene(rootLayout);

            // Add stylesheet to prevent San Fransisco Font Issue on macOS
            scene.getStylesheets().add(getClass().getResource("styles/font-fix.css").toExternalForm());

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Populates the rootLayout with the PersonOverview view
     */
    public void showPersonOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/PersonOverview.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(personOverview);

            // Give the controller access to the main app.
            PersonOverviewController controller = loader.getController();
            controller.setMain(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean showPersonEditDialog(Person person) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/PersonEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Person Bearbeiten");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);

            Scene scene = new Scene(page);
            scene.getStylesheets().add(getClass().getResource("styles/font-fix.css").toExternalForm());
            dialogStage.setScene(scene);

            PersonEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPerson(person);

            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();;
            return false;
        }
    }
}
