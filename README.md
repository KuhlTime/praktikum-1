### Praktikum 1 - Software Engineering 2

- Name: **Andr� Kuhlmann**
- Matr.-Nr: **779690**
- Erstellt: **24. April 2020**
- Java: **11**
- IDE: **[IntelliJ IDEA](https://www.jetbrains.com/de-de/idea/)**


#### Aufgaben:  
1. Erstellen Sie mit Eclipse ein neues JavaFX-Projekt
2. Erstellen Sie eine Benutzeroberfl�che gem�� des [Tutorials](http://code.makery.ch/library/javafx-8-tutorial/) von Schritt 1 bis einschlie�lich Schritt 3 
